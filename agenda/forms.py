from django import forms
from django.utils import timezone
from datetime import datetime, date

class Agenda_Form(forms.Form):
    waktu_acara = forms.DateTimeField(required=True, widget=forms.DateTimeInput())
    kegiatan_acara = forms.CharField(required=True, max_length=100, widget=forms.TextInput())
    tempat_acara = forms.CharField(required=True, max_length=100, widget=forms.TextInput())
    kategori_acara = forms.CharField(required=True, max_length=100, widget=forms.TextInput())
