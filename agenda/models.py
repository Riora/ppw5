from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.
class Acara(models.Model):
    waktu_acara = models.DateTimeField()
    kegiatan_acara = models.CharField(max_length=100)
    tempat_acara = models.CharField(max_length=100)
    kategori_acara = models.CharField(max_length=100)

    def __str__(self):
        return self.kegiatan_acara