from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.
class Acara(models.Model):
    waktu = models.DateTimeField()
    kegiatan = models.CharField(max_length=100)
    tempat = models.CharField(max_length=100)
    kategori = models.CharField(max_length=100)

    def __str__(self):
        return self.kegiatan