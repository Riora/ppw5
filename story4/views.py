from django.shortcuts import render
from datetime import datetime
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from . import templates
from . import forms
from .models import Acara
from .forms import Agenda_Form
from django.template import loader

response = {}

def homepage(request):
    return render(request, 'first.html')

def resumePage(request):
    return render(request, 'second.html')

def guestPage(request):
    return render(request, 'challange.html')

def agendaPage(request):
    try :
        return render(request, 'agenda.html',{'list_acara' : Acara.objects.all()})
    except Exception as e :
        return render(request, 'agenda.html',{})

def agendaForm(request):
    try:
        return render(request, 'agenda_form.html', {'Form' : Acara})
    except Exception  as e:
        return render(request,'agenda_form.html')

def zenmetsu(request):
    Acara.objects.all().delete()
    return HttpResponseRedirect('../agenda/')
    #return HttpResponse(request, 'agenda.html')

def agendaPost(request):

    form = Agenda_Form(request.POST or None)

    if(request.method == 'POST' and form.is_valid()):
        response['form'] = form
        response['waktu_acara'] = request.POST['waktu_acara'] if request.POST['waktu_acara'] != "" else "waktu tidak diketahui"
        response['kegiatan_acara'] = request.POST['kegiatan_acara'] if request.POST['kegiatan_acara'] != "" else "Kegiatan tidak diketahui"
        response['tempat_acara'] = request.POST['tempat_acara'] if request.POST['tempat_acara'] != "" else "Tempat tidak diketahui"
        response['kategori_acara']= request.POST['kategori_acara'] if request.POST['kategori_acara'] != "" else "Kategori tidak diketahui"
        acara = Acara(waktu = response['waktu_acara'], kegiatan = response['kegiatan_acara'],
                      tempat = response['tempat_acara'], kategori = response['kategori_acara'])
        acara.save()
        achara = Acara.objects.all()
        return render(request, 'agenda.html', {'list_acara' : achara})
    else:
        return render(request, 'agenda_form.html', {'form' : form})



# Create your views here.
