from django import forms
from django.utils import timezone
from datetime import datetime, date

attrs = {
    'class': 'form-control'
    }
class Agenda_Form(forms.Form):
    waktu_acara = forms.DateTimeField(required=True, widget=forms.DateTimeInput(attrs=attrs))
    kegiatan_acara = forms.CharField(required=True, max_length=100, widget=forms.TextInput(attrs=attrs))
    tempat_acara = forms.CharField(required=True, max_length=100, widget=forms.TextInput(attrs=attrs))
    kategori_acara = forms.CharField(required=True, max_length=100, widget=forms.TextInput(attrs=attrs))
